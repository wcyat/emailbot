FROM python:3.10 as build

WORKDIR /usr/src/app

COPY *.py requirements.txt ./

RUN python -m venv venv

RUN . venv/bin/activate && pip install -r requirements.txt

FROM python:3.10-alpine

WORKDIR /usr/src/app

COPY *.py ./
COPY --from=build /usr/src/app/venv ./venv/

CMD . venv/bin/activate && python bot.py
